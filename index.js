const TelegramBot = require('node-telegram-bot-api');

const express = require('express');
const app = express();


const token = `6471607300:AAGC-LxzzP4dKH_di9ef_bDUWbtdLfY_o-k`;

const bot = new TelegramBot(token, {polling: true});

let gameOwner = "kimse degil amk";
let randomNumber;

let language = "eniste"
bot.onText(/\/start/, (msg) => {
  if (language === "en") {
    bot.sendMessage(msg.chat.id, "• Hello 📖\n\n• I'm a Game Bot 🎮\n\n• You can play with me to play calculation game and have fun ✍🏻\n\n• To play with me you need to add me to a group and make me an admin . 💭\n\n/help FOR HELP MESSAGE\n\n° YOU CAN START THE GAME WITH THE COMMAND /game");
  } else if (language === "tr") {
    bot.sendMessage(msg.chat.id, "• Merhaba 📖\n\n• Ben bir Oyun Botuyum 🎮\n\n• Hesaplama oyunu oynamak ve eğlenmek için benimle oynayabilirsiniz. ✍🏻\n\n• Benimle oynamak için beni bir gruba ekleyip yönetici yapmalısın. 💭\n\n/help YARDIM MESAJI İÇİN\n\n° /oyun KOMUTU İLE OYUNA BAŞLAYABİLİRSİNİZ");
  } else if (language === "es") {
    bot.sendMessage(msg.chat.id, "• Hola 📖\n\n• Soy un Game Bot 🎮\n\n• Puedes jugar conmigo para jugar al juego de cálculo y divertirte. ✍🏻\n\n• Para jugar conmigo, tienes que agregarme a un grupo y hacerme administrador. 💭\n\n° PUEDES EMPEZAR EL JUEGO CON EL COMANDO /game\n\n/help FOR HELP MESSAGE");
  } else {
    bot.sendMessage(msg.chat.id, "Sorry you didn't set your language. To set your language, you can type:\n\n/en\n/tr\n/es");
  }
});

bot.onText(/\/en/, (msg) => {
  language = "en"
  bot.sendMessage(msg.chat.id, "New Language = ENGLISH")
});

bot.onText(/\/tr/, (msg) => {
  language = "tr"
  bot.sendMessage(msg.chat.id, "Yeni diliniz = TÜRKÇE")
});

bot.onText(/\/es/, (msg) => {
  language = "es"
  bot.sendMessage(msg.chat.id, "Tu nuevo idioma = ESPAÑA")
});


bot.onText(/\/game/, (msg) => {
  if (gameOwner !== "kimse degil amk") {
    bot.sendMessage(msg.chat.id, "Oyun zaten başlatılmış.🚨❌")
    return;
    }
  gameOwner = msg.from.id;
  randomNumber = Math.floor(Math.random() * 1000);
  bot.sendMessage(msg.chat.id, `${msg.from.first_name} şimdi sayıyı hesaplama şeklinde anlatacak.`, {
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: 'Rakama göz at👀',
            callback_data: 'showNumber'
          }
        ],
        [
          {
            text: '❌ Beceremiyorum! Başkasına devret',
            callback_data: 'devret'
          }
        ]
      ]
    }
  });
});

bot.onText(/\/number/, (msg) => {
  if (msg.from.id !== gameOwner) {
    bot.sendMessage(msg.from.id, "Sen anlatıcı değilsin!❌")
    return
};
  if (msg.from.id === gameOwner) {
     bot.sendMessage(msg.chat.id, `${msg.from.first_name}, Sayını görüntülemek istiyorsan aşağıdaki tuşa bas!`, {
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: 'Benim sayım ne?👀',
            callback_data: 'showNumberr'
          }
        ],
        [
          {
            text: '❌ Beceremiyorum! Başkasına devret',
            callback_data: 'devret'
          }
        ]
      ]
    }
  });
  }});


bot.on('callback_query', (query) => {
  randomNumber = Math.floor(Math.random() * 1000);
  if (query.data === 'showNumberr' && query.from.id !== gameOwner) {
    bot.answerCallbackQuery(query.id, {
  text: `Sen anlatıcı değilsin!❌`,
  show_alert: true
});
   
    return
  } 
  if (query.data === 'showNumberr' && query.from.id === gameOwner) {
    bot.answerCallbackQuery(query.id, {
  text: `Sayı: ${randomNumber}`,
  show_alert: true
});
    
  }
 
});

           
bot.on('callback_query', (query) => {
  if (query.data === 'showNumber' && query.from.id !== gameOwner) {
    bot.answerCallbackQuery(query.id, {
  text: `Sen anlatıcı değilsin!❌`,
  show_alert: true
});
   
    return
  } 
  if (query.data === 'showNumber' && query.from.id === gameOwner) {
    bot.answerCallbackQuery(query.id, {
  text: `Sayı: ${randomNumber}`,
  show_alert: true
});
    
  }
 
});
bot.on('callback_query', (query) => {
    bot.answerCallbackQuery(query.id, {
        text: `Devretmek için /devret yaz.😅💡`,
        show_alert: true
});
    
  
    
  
});
bot.onText(/\/devret/, (msg) => {

  if (msg.from.id == gameOwner) {
    gameOwner = "kimse degil amk";
    bot.sendMessage(msg.chat.id, 'Oyun devrediliyor. Almak isteyen /game yazsın.🙃😁');
    }
  
});

    
 





bot.onText(/\d+/, (msg) => {
  if (msg.from.id !== gameOwner && parseInt(msg.text) === randomNumber) {
    gameOwner = msg.from.id;
    bot.sendMessage(msg.chat.id, `Tebrikler✅ ${msg.from.first_name}, rakamı buldunuz✔️... Şimdi sana anlatman için bir rakam oluşturdum. ${msg.from.first_name} /number komutu ile rakamına bakabilirsin.!💬`);
  }
});

bot.onText(/\help/, (msg) => {
    bot.sendMessage(msg.chat.id, `MEVCUT KOMUTLARIM:

/game : Oyunu başlatır.
/end : Oyunu bitirir.
/number : Anlatmanız gereken  sayı . . .
/gpoints : Global puanları gösterir.(Şuanlık mevcut değil.)`);  
});

bot.onText(/\end/, (msg) => {
	gameOwner = "kimse degil amk"
    bot.sendMessage(msg.chat.id, `❌ Oyun @${msg.from.username} tarafından durduruldu.
OYUN KANALIMIZ: @calcgame`);  
	});

app.get('/', (req, res) => {
  res.send('Selam Dünya!');
});

app.listen(3000, () => {
  console.log('Sunucu 3000 numaralı bağlantı noktasında çalışıyor.');
});
